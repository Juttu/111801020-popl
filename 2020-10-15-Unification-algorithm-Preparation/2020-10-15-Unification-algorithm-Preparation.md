(*1*)


signature SIG = sig
	(*type of function symbols*)
	type symbol
		(* the arity function*)
		val arity: symbol -> int 
(* The additional constraint that the Ord.ord_key type is the same as the type of functional symbols*)	
structure Ord: ORD_KEY
	       where type ord_key=symbol
	       end
	       
(* Defining the SML signature to look at signature set which is in the set of functional symbols and arity *)
signature VAR = sig 
	   type var 
	   structure Ord:ORD_KEY 
	   		where type ord_key = var 
	   	   end 
	   	   
	   	   
(* 2 *)

functor Term (structure S:SIG ; structure V:VAR)
=
struct 
		datatype term = ot of S.symbol | var of V.var | app of S.symbol * term list
		fun occur(w:term,e:V.var):bool = case w of ot(a) => false 
						
						
						| var(a) => V.Ord.compare(e,a)=EQUAL
						| app(_,y) => List.exists(fn a => occur(a,e)) y
		
		
		
		
	end
