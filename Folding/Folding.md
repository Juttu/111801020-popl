(*1*)

fun foldl (sfun : 'a* 'b -> 'b) (s0:'b) [] =s0
  | foldl sfun s0 (x::xs) = foldl sfun(sfun(x,s0)) xs;
fun foldr (sfun : 'a* 'b -> 'b) (s0:'b) []=s0
  | foldr sfun s0 (x::xs) = sfun (x,(foldr sfun s0 xs));

(*2*)

fun sum (lst:int list) : int = foldl (fn(x,s0) =>x+s0) 0 lst
