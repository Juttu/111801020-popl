(*1*)
fun curry f x y z = f(x,y,z);
(*Curried fn: ('a * 'b * 'c -> 'd) -> 'a -> 'b -> 'c -> 'd *)
fun uncurry f(x,y,z)= f x y z;
(*Uncurried fn: ('a -> 'b -> 'c -> 'd) -> 'a * 'b * 'c -> 'd *)
(*2*)
fun fst(a,_)=a;
fun snd(_,b)=b;

(*3*)
fun length nil = 0
  | length (a::an) = 1+ length an;

(*4*)
fun rev(l) = 
	    let
	        fun rev1([],l) = l | rev1(h::t,l) = rev1(t,h::l)
	    in
	        rev1(l,[])
	    end;
(*5*)
fun fib(n) = 
	    let
	        fun fib1(a,b,1) = b | fib1(a,b,n) = fib1(a+b,a,n-1)
	    in
	        fib1(1,0,n)
	    end;
