


(*Q1*)
datatype hlam = HV of string
          | HA of hlam * hlam
          | HL of hlam -> hlam


fun subst (x,u) (HV(q)) = if (x = q) then u else (HV(q))
  | subst (x,u) (HA(l,m)) = HA((subst(x,u) l),(subst (x,u) m))
  | subst (x,u) (HL(f)) = let fun fp t = subst (x,u) (f t) in HL (fp) end

(*Q2*)
fun abstract x M = let fun f t = case M of (HV(l)) => subst(x,t) (HV(l))
                         | (HA(l,m)) => subst(x,t) (HA(l,m))
                         | (HL(f)) => subst(x,t) (HL(f))
                    in HL(f) end
