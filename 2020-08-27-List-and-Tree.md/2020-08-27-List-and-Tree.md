(*Questionno1*)
fun map f [] = []
  | map f (x::xs) = f x :: map f xs;
(*Questionno2*)
datatype 'a tree = nil | cons of ('a * 'a tree * 'a tree);
(*Question03*)
fun treemap f nil = nil | treemap f (cons(root,left,right)) = cons(f root,treemap f left,treemap f right);
(*Question04*)
fun preorder nil = []

  | preorder (cons(root,left,right)) = [root] @ (preorder left) @ (preorder right);


fun inorder nil = []

 | inorder (cons(root,left,right)) = (inorder left) @ [root] @ (inorder right);

fun postorder nil = []

  | postorder (cons(root,left,right)) = (postorder left) @ (postorder right) @ [root];
(*Question05*)
fun rotcw (cons(root,cons(root1,left1,right1),right)) = cons(root1,left1,cons(root,right1,right)) | rotcw x = x;
 

