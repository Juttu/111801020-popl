(*Q1*)

fun delete (s,[]) = []

| delete (s,x::xs') = 

if s = x then delete(s,xs') 

else x::delete(s, xs')

 



(* The three variaties are
    variables,function applications,lambda abstractions *)
datatype expr=Va of string
	     |Application of expr * expr
	     |Abstraction of string * expr





(*Q3*)

fun free (e:expr)=case e of

Va x=>[x] (*if we have only one variable*)
 | Application(e1,e2) =>free e1 @ free e2 (* union of two free variables in those expressions*)


 | Abstraction(y,ey) =>delete(y,free ey)   (*we use delete function which we have declared before, deleting y from the free variables in ey*)
